exports.config = {
  bundles: [{
    components: [
      'my-name',
      'foundation-page',

      'foundation-accordion',
      'foundation-accordion-menu',
      'foundation-badge',
      'foundation-breadcrumbs',
      'foundation-callout',
      'foundation-close',
      'foundation-drilldown-menu',
      'foundation-dropdown-menu',
      'foundation-dropdown-pane',
      'foundation-equalizer',
      'foundation-form',
      'foundation-interchange',
      'foundation-label',
      'foundation-magellan',
      'foundation-media-object',
      'foundation-menu',
      'foundation-off-canvas',
      'foundation-orbit',
      'foundation-pagination',
      'foundation-progress-bar',
      'foundation-responsive-embed',
      'foundation-reveal',
      'foundation-slider',
      'foundation-sticky',
      'foundation-switch',
      'foundation-table',
      'foundation-tabs',
      'foundation-thumbnail',
      'foundation-tooltip',
      'foundation-top-bar'
    ]
  }],
  collections: [{
    name: '@stencil/router'
  }]
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
};
