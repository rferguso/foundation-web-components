import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-menu',
  styleUrl: 'foundation-menu.scss'
})
export class FoundationMenu {
  render() {
    return (
      <ul class="menu">
        <li><a href="#0"><i class="fi-list"></i> <span>One</span></a></li>
        <li><a href="#0"><i class="fi-list"></i> <span>Two</span></a></li>
        <li><a href="#0"><i class="fi-list"></i> <span>Three</span></a></li>
        <li><a href="#0"><i class="fi-list"></i> <span>Four</span></a></li>
      </ul>
    );
  }
}