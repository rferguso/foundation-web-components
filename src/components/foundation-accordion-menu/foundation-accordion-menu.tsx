import { Component, Element, Prop } from '@stencil/core';
import $ from 'jquery';

import { Foundation } from 'foundation-sites/js/foundation.core';
Foundation.addToJquery($);

import { AccordionMenu } from 'foundation-sites/js/foundation.accordionMenu';
Foundation.plugin(AccordionMenu, 'AccordionMenu');

@Component({
  tag: 'foundation-accordion-menu',
  styleUrl: 'foundation-accordion-menu.scss'
})

export class FoundationAccordionMenu {
  @Element() accordionMenuElement: HTMLElement;
  @Prop() content: any[];

  componentDidLoad() {
    new Foundation.AccordionMenu($(this.accordionMenuElement).children('ul:first-child'), {
      allowAllClosed: true
    });
  }

  printList(arr, sub) {
    if (sub) {
      return (
        <ul>
          {
            arr.map(item => {
              return (
                <li>
                  <a href={item.url}>{item.title}</a>
                  {
                    item.submenu ? this.printList(item.submenu, true) : null
                  }
                </li>
              )
            })
          }
        </ul>
      )
    } else {
      return (
        arr.map(item => {
          return (
            <li>
              <a href={item.url}>{item.title}</a>
              {
                item.submenu ? this.printList(item.submenu, true) : null
              }
            </li>
          )
        })
      )
    }
  }

  render() {
    console.log('render accordion menu');
    return (
      <ul class="accordion-menu" data-accordion-menu>
        {this.printList(this.content, null)}
      </ul>
    );
  }
}