import { Component, Prop, Element } from '@stencil/core';
import $ from 'jquery';

import { Foundation } from 'foundation-sites/js/foundation.core';
Foundation.addToJquery($);

import { Drilldown } from 'foundation-sites/js/foundation.drilldown';
Foundation.plugin(Drilldown, 'Drilldown');

@Component({
  tag: 'foundation-drilldown-menu',
  styleUrl: 'foundation-drilldown-menu.scss'
})
export class FoundationDrilldownMenu {
  @Prop() content: any[];
  @Element() drilldownMenuElement: HTMLElement;

  componentDidLoad() {
    new Foundation.Drilldown($(this.drilldownMenuElement).find('.drilldown'), {
      autoHeight: true,
      animateHeight: true
    });
  }

  printList(arr, sub) {
    if (sub) {
      return (
        <ul class="">
          {
            arr.map(item => {
              return (
                <li>
                  <a href={item.url}>{item.title}</a>
                  {
                    item.submenu ? this.printList(item.submenu, true) : null
                  }
                </li>
              )
            })
          }
        </ul>
      )
    } else {
      return (
        arr.map(item => {
          return (
            <li>
              <a href={item.url}>{item.title}</a>
              {
                item.submenu ? this.printList(item.submenu, true) : null
              }
            </li>
          )
        })
      )
    }
  }
  render() {
    console.log('render drilldown');
    return (
      <div class="is-drilldown">
        <ul class="drilldown" data-drilldown>
          {this.printList(this.content, null)}
        </ul>
      </div>
    );
  }
}