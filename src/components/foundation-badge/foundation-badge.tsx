import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'foundation-badge',
  styleUrl: 'foundation-badge.scss'
})
export class FoundationBadge {
  @Prop() text: string;
  @Prop() type: string;

  render() {
    return (
      <span class={this.type}>{this.text}</span>
    );
  }
}