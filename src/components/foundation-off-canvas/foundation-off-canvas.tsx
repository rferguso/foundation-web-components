import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-off-canvas',
  styleUrl: 'foundation-off-canvas.scss'
})
export class FoundationOffCanvas {
  render() {
    return (
      <div />
    );
  }
}