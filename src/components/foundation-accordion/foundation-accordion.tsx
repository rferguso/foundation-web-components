import { Component, Element } from '@stencil/core';
import $ from 'jquery';

import { Foundation } from 'foundation-sites/js/foundation.core';
Foundation.addToJquery($);

import { Accordion, Prop } from 'foundation-sites/js/foundation.accordion';
Foundation.plugin(Accordion, 'Accordion');

@Component({
  tag: 'foundation-accordion',
  styleUrl: 'foundation-accordion.scss'
})
export class FoundationAccordion {
  @Element() accordionElement: HTMLElement;
  @Prop() content: any[];

  componentDidLoad() {
    new Foundation.Accordion($(this.accordionElement).children('.accordion'), {
      allowAllClosed: true,
    });
  }

  render() {
    console.log('render accordion');
    return (
      <ul class="accordion" data-accordion>
        {
          this.content.map((item) => {
            return (
              <li class="accordion-item" data-accordion-item>
                <a href="#" class="accordion-title">{item.title}</a>
                <div class="accordion-content" data-tab-content innerHTML={item.content}></div>
              </li>
            )
          })
        }
      </ul>
    );
  }
}





