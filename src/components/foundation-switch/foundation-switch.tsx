import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-switch',
  styleUrl: 'foundation-switch.scss'
})
export class FoundationSwitch {
  render() {
    return (
      [<div class="switch tiny">
        <input class="switch-input" id="tinySwitch" type="checkbox" name="exampleSwitch" />
        <label class="switch-paddle" htmlFor="tinySwitch">
          <span class="show-for-sr">Tiny Sandwiches Enabled</span>
        </label>
      </div>,

      <div class="switch small">
        <input class="switch-input" id="smallSwitch" type="checkbox" name="exampleSwitch" />
        <label class="switch-paddle" htmlFor="smallSwitch">
          <span class="show-for-sr">Small Portions Only</span>
        </label>
      </div>,

      <div class="switch large">
        <input class="switch-input" id="largeSwitch" type="checkbox" name="exampleSwitch" />
        <label class="switch-paddle" htmlFor="largeSwitch">
          <span class="show-for-sr">Show Large Elephants</span>
        </label>
      </div>]
    );
  }
}