import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-table',
  styleUrl: 'foundation-table.scss'
})
export class FoundationTable {
  render() {
    const style_a = { width: '200px' };
    const style_b = { width: '150px' };
    return (
      <table>
        <thead>
          <tr>
            <th style={style_a}>Table Header</th>
            <th>Table Header</th>
            <th style={style_b}>Table Header</th>
            <th style={style_b}>Table Header</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Content Goes Here</td>
            <td>This is longer content Donec id elit non mi porta gravida at eget metus.</td>
            <td>Content Goes Here</td>
            <td>Content Goes Here</td>
          </tr>
          <tr>
            <td>Content Goes Here</td>
            <td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
            <td>Content Goes Here</td>
            <td>Content Goes Here</td>
          </tr>
          <tr>
            <td>Content Goes Here</td>
            <td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
            <td>Content Goes Here</td>
            <td>Content Goes Here</td>
          </tr>
        </tbody>
      </table>
    );
  }
}