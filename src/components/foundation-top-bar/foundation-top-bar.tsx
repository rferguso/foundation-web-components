import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-top-bar',
  styleUrl: 'foundation-top-bar.scss'
})
export class FoundationTopBar {
  render() {
    return (
      <div class="top-bar">
        <div class="top-bar-left">
          <ul class="dropdown menu" data-dropdown-menu>
            <li class="menu-text">Site Title</li>
            <li class="has-submenu">
              <a href="#0">One</a>
              <ul class="submenu menu vertical" data-submenu>
                <li><a href="#0">One</a></li>
                <li><a href="#0">Two</a></li>
                <li><a href="#0">Three</a></li>
              </ul>
            </li>
            <li><a href="#0">Two</a></li>
            <li><a href="#0">Three</a></li>
          </ul>
        </div>
        <div class="top-bar-right">
          <ul class="menu">
            <li><input type="search" placeholder="Search" /></li>
            <li><button type="button" class="button">Search</button></li>
          </ul>
        </div>
      </div>
    );
  }
}