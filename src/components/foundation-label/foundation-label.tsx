import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-label',
  styleUrl: 'foundation-label.scss'
})
export class FoundationLabel {
  render() {
    return (
      [<span class="primary label">Primary Label</span>,
      <span class="secondary label">Secondary Label</span>,
      <span class="success label">Success Label</span>,
      <span class="alert label">Alert Label</span>,
      <span class="warning label">Warning Label</span>]
    );
  }
}