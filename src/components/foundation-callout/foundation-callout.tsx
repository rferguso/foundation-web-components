import { Component, Prop, Element } from '@stencil/core';


@Component({
  tag: 'foundation-callout',
  styleUrl: 'foundation-callout.scss'
})
export class FoundationCallout {
  @Prop() content: string;
  @Prop() type: string;
  @Element() calloutElement: HTMLElement;

  handleClose(event: UIEvent) {
    console.log('closing......');
    this.calloutElement.classList.add('closed');
  }
  render() {
    return (
      <div data-closable class={`${this.type} callout`} innerHTML={this.content} >
        <foundation-close onClick={this.handleClose.bind(this)} />
      </div>
    );
  }
}