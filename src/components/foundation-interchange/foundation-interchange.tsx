import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-interchange',
  styleUrl: 'foundation-interchange.scss'
})
export class FoundationInterchange {
  render() {
    return (
      <img data-interchange="[assets/img/interchange/small.jpg, small], [assets/img/interchange/medium.jpg, medium], [assets/img/interchange/large.jpg, large]" />
    );
  }
}