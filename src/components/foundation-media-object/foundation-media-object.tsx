import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-media-object',
  styleUrl: 'foundation-media-object.scss'
})
export class FoundationMediaObject {
  render() {
    return (
      [<div class="media-object">
        <div class="media-object-section">
          <img src="http://placeimg.com/200/200/people" />
        </div>,
          <div class="media-object-section">
          <h4>Dreams feel real while we're in them.</h4>
          <p>I'm going to improvise. Listen, there's something you should know about me... about inception. An idea is like a virus, resilient, highly contagious. The smallest seed of an idea can grow. It can grow to define or destroy you.</p>
        </div>
      </div>]
    );
  }
}