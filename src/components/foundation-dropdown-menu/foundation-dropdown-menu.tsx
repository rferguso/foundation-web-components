import { Component, Prop, Element } from '@stencil/core';
import $ from 'jquery';

import { Foundation } from 'foundation-sites/js/foundation.core';
Foundation.addToJquery($);

import { DropdownMenu } from 'foundation-sites/js/foundation.dropdownMenu';
Foundation.plugin(DropdownMenu, 'DropdownMenu');


@Component({
  tag: 'foundation-dropdown-menu',
  styleUrl: 'foundation-dropdown-menu.scss'
})
export class FoundationDropdownMenu {
  @Prop() content: any[];
  @Prop() orientation: string;
  @Element() dropdownMenuElement: HTMLElement;

  classArray = [
    'dropdown',
    'menu'
  ];

  componentWillLoad() {
    this.orientation === 'vertical' ? this.classArray.push(this.orientation) : null;
  }
  componentDidLoad() {
    new Foundation.DropdownMenu($(this.dropdownMenuElement).children('ul:first-child'), {});
  }

  printList(arr, sub) {
    if (sub) {
      return (
        <ul class="menu nested">
          {
            arr.map(item => {
              return (
                <li>
                  <a href={item.url}>{item.title}</a>
                  {
                    item.submenu ? this.printList(item.submenu, true) : null
                  }
                </li>
              )
            })
          }
        </ul>
      )
    } else {
      return (
        arr.map(item => {
          return (
            <li>
              <a href={item.url}>{item.title}</a>
              {
                item.submenu ? this.printList(item.submenu, true) : null
              }
            </li>
          )
        })
      )
    }
  }

  render() {
    return (
      <ul class={this.classArray.join(' ')} data-dropdown-menu>
        {this.printList(this.content, null)}
      </ul>
    );
  }
}