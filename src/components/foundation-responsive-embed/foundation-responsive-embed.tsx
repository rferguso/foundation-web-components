import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-responsive-embed',
  styleUrl: 'foundation-responsive-embed.scss'
})
export class FoundationResponsiveEmbed {
  render() {
    return (
      <div class="responsive-embed">
        <iframe width="420" height="315" src="https://www.youtube.com/embed/mM5_T-F1Yn4" frameBorder={0} allowFullScreen></iframe>
      </div>
    );
  }
}