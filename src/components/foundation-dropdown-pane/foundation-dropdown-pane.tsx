import { Component, Prop, Element } from '@stencil/core';


@Component({
  tag: 'foundation-dropdown-pane',
  styleUrl: 'foundation-dropdown-pane.scss'
})
export class FoundationDropdownPane {
  @Element() dropdownPaneElement: HTMLElement;
  @Prop() button: string;
  @Prop() content: string;

  handleClick() {
    // this.dropdownPaneElement.childNodes[1].classList.toggle('active');
    this.dropdownPaneElement.children[1].classList.toggle('active');
  }

  render() {
    return ([
      <button
        onClick={this.handleClick.bind(this)}
        class="button"
        type="button"
        data-toggle="example-dropdown">
        {this.button}
      </button>,
      <div class="dropdown-pane" id="example-dropdown" data-dropdown innerHTML={this.content} />
    ]);
  }
}