import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-close',
  styleUrl: 'foundation-close.scss'
})
export class FoundationClose {
  render() {
    return (
      <button class="close-button" aria-label="Close alert" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
    );
  }
}