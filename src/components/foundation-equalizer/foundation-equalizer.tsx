import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-equalizer',
  styleUrl: 'foundation-equalizer.scss'
})
export class FoundationEqualizer {
  render() {
    return (
      <div class="row" data-equalizer data-equalize-on="medium" id="test-eq">
        <div class="medium-4 columns">
          <div class="callout" data-equalizer-watch>
            <img src="assets/img/generic/square-1.jpg" />
          </div>
        </div>

        <div class="medium-4 columns">
          <div class="callout" data-equalizer-watch>
            <p>Pellentesque habitant morbi tristique senectus et netus et, ante.</p>
          </div>
        </div>

        <div class="medium-4 columns">
          <div class="callout" data-equalizer-watch>
            <img src="assets/img/generic/rectangle-1.jpg" />
          </div>
        </div>
      </div>
    );
  }
}