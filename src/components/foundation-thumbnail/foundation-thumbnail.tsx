import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-thumbnail',
  styleUrl: 'foundation-thumbnail.scss'
})
export class FoundationThumbnail {
  render() {
    return (
      <div class="row">
        <div class="small-4 columns">
          <img class="thumbnail" src="assets/img/thumbnail/01.jpg" alt="Photo of Uranus." />
        </div>
        <div class="small-4 columns">
          <img class="thumbnail" src="assets/img/thumbnail/02.jpg" alt="Photo of Neptune." />
        </div>
        <div class="small-4 columns">
          <img class="thumbnail" src="assets/img/thumbnail/03.jpg" alt="Photo of Pluto." />
        </div>
      </div>
    );
  }
}