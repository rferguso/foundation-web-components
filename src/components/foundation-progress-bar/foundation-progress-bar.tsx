import { Component } from '@stencil/core';


@Component({
  tag: 'foundation-progress-bar',
  styleUrl: 'foundation-progress-bar.scss'
})
export class FoundationProgressBar {
  render() {
    const style_a = { width: '25%' };
    const style_b = { width: '50%' };
    const style_c = { width: '75%' };
    const style_d = { width: '100%' };
    return (
      [<div class="primary progress" role="progressbar" tabIndex={0} aria-valuenow="25" aria-valuemin="0" aria-valuetext="25 percent" aria-valuemax="100">
        <div class="progress-meter" style={style_a}>
          <p class="progress-meter-text">25%</p>
        </div>
      </div >,

      <div class="warning progress">
        <div class="progress-meter" style={style_b}>
          <p class="progress-meter-text">50%</p>
        </div>
      </div >,

      <div class="alert progress">
        <div class="progress-meter" style={style_c}>
          <p class="progress-meter-text">75%</p>
        </div>
      </div >,

      <div class="success progress" role="progressbar" tabIndex={0} aria-valuenow="100" aria-valuemin="0" aria-valuetext="100 percent" aria-valuemax="100">
        <div class="progress-meter" style={style_d}>
          <p class="progress-meter-text">100%</p>
        </div>
      </div >]
    );
  }
}