import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'foundation-breadcrumbs',
  styleUrl: 'foundation-breadcrumbs.scss'
})
export class FoundationBreadcrumbs {
  @Prop() content: any[];

  render() {
    return (
      <nav aria-label="You are here:" role="navigation">
        <ul class="breadcrumbs">
          {
            this.content.map(item => {
              return (
                <li><a href={item.url}>{item.title}</a></li>
              )
            })
          }
        </ul>
      </nav>
    );
  }
}

// <li><a href="#0">Features</a></li>
// <li class="disabled">Gene Splicing</li>
// <span class="show-for-sr">Current: </span>