import { Component } from '@stencil/core';

@Component({
  tag: 'foundation-page',
  styleUrl: 'foundation-page.scss'
})
export class FoundationPage {
  render() {
    return (
      <div>
        <h1>Foundation Components</h1>
        <my-name first="Stencil" last="JS"></my-name>

        <section>
          <h2>Accordion</h2>
          <foundation-accordion content={[
            {
              "title": "Accordion 1",
              "content": `
              <p>Panel 1. Lorem ipsum dolor</p>
              <a href=\"#\">Nowhere to Go</a>`
            },
            {
              "title": "Accordion 2",
              "content": `
              <textarea></textarea>
              <button class=\"button\">I do nothing!</button>`
            },
            {
              "title": "Accordion 3",
              "content": `
              Type your name!
              <input type=\"text\"></input>`
            }
          ]} />
        </section>

        <section>
          < h2 > Accordion Menu</h2 >
          <foundation-accordion-menu content={
            [
              {
                'title': 'Item 1',
                'url': '#',
                'submenu': [
                  {
                    'title': 'Item 1A',
                    'url': '#',
                    'submenu': [
                      {
                        'title': 'Item 1Ai',
                        'url': '#',
                      },
                      {
                        'title': 'Item 1Aii',
                        'url': '#',
                      },
                      {
                        'title': 'Item 1Aiii',
                        'url': '#',
                      },
                    ]
                  },
                  {
                    'title': 'Item 1B',
                    'url': '#',
                  },
                  {
                    'title': 'Item 1C',
                    'url': '#',
                  },
                ]
              },
              {
                'title': 'Item 2',
                'url': '#',
                'submenu': [
                  {
                    'title': 'Item 2A',
                    'url': '#',
                  },
                  {
                    'title': 'Item 2B',
                    'url': '#',
                  },
                ]
              },
              {
                'title': 'Item 3',
                'url': '#'
              },
            ]
          } />
        </section>

        <section>
          <h2>Badge</h2>
          <foundation-badge type="primary" text="1" />
          <foundation-badge type="secondary" text="2" />
          <foundation-badge type="success" text="3" />
          <foundation-badge type="alert" text="A" />
          <foundation-badge type="warning" text="B" />
        </section>

        <section>
          <h2>Breadcrumbs</h2>
          <foundation-breadcrumbs content={[
            {
              "title": "Home",
              "url": "/"
            },
            {
              "title": "Features",
              "url": "/Features"
            },
            {
              "title": "Gene Splicing",
              "url": "/Features/GeneSplicing"
            }
          ]} />
        </section>

        <section>
          <h2>Callout & Close Button</h2>
          <foundation-callout type="" content={`
            <h5>This is a callout.</h5>
            <p>It has an easy to override visual style, and is appropriately subdued.</p>
            <a href="#0">It's dangerous to go alone, take this.</a>
          `} />
          <foundation-callout type="primary" content={`
          <h5>This is a primary callout.</h5>
          <p>It has an easy to override visual style, and is appropriately subdued.</p>
          <a href="#0">It's dangerous to go alone, take this.</a>
          `} />
          <foundation-callout type="secondary" content={`
          <h5>This is a secondary callout.</h5>
          <p>It has an easy to override visual style, and is appropriately subdued.</p>
          <a href="#0">It's dangerous to go alone, take this.</a>
          `} />
          <foundation-callout type="success" content={`
          <h5>This is a success callout.</h5>
          <p>It has an easy to override visual style, and is appropriately subdued.</p>
          <a href="#0">It's dangerous to go alone, take this.</a>
          `} />
          <foundation-callout type="warning" content={`
          <h5>This is a warning callout.</h5>
          <p>It has an easy to override visual style, and is appropriately subdued.</p>
          <a href="#0">It's dangerous to go alone, take this.</a>
          `} />
          <foundation-callout type="alert" content={`
          <h5>This is a alert callout.</h5>
          <p>It has an easy to override visual style, and is appropriately subdued.</p>
          <a href="#0">It's dangerous to go alone, take this.</a>
          `} />
        </section>

        <section>
          <h2>Drilldown Menu</h2>
          <foundation-drilldown-menu content={[
            {
              "url": "#0",
              "title": "Item 1",
              "submenu": [
                {
                  "url": "#0",
                  "title": "Item 1A",
                  "submenu": [
                    {
                      url: "#0",
                      title: "Item 1Aa"
                    },
                    {
                      url: "#0",
                      title: "Item 1Ba"
                    },
                    {
                      url: "#0",
                      title: "Item 1Ca"
                    },
                    {
                      url: "#0",
                      title: "Item 1Da"
                    },
                    {
                      url: "#0",
                      title: "Item 1Ea"
                    }
                  ]
                },
                {
                  "url": "#0",
                  "title": "Item 1B"
                },
                {
                  "url": "#0",
                  "title": "Item 1C"
                },
                {
                  "url": "#0",
                  "title": "Item 1D"
                },
                {
                  "url": "#0",
                  "title": "Item 1E"
                }
              ]
            },
            {
              "url": "#0",
              "title": "Item 2",
              "submenu": [
                {
                  url: "#0",
                  title: "Item 2A"
                },
                {
                  url: "#0",
                  title: "Item 2B"
                },
                {
                  url: "#0",
                  title: "Item 2C"
                },
                {
                  url: "#0",
                  title: "Item 2D"
                },
                {
                  url: "#0",
                  title: "Item 2E"
                }
              ]
            },
            {
              "url": "#0",
              "title": "Item 3",
              "submenu": [
                {
                  url: "#0",
                  title: "Item 3A"
                },
                {
                  url: "#0",
                  title: "Item 3B"
                },
                {
                  url: "#0",
                  title: "Item 3C"
                },
                {
                  url: "#0",
                  title: "Item 3D"
                },
                {
                  url: "#0",
                  title: "Item 3E"
                }
              ]
            },
            {
              "url": "#0",
              "title": "Item 4"
            },
          ]} />
        </section >

        <section>
          <h2>Dropdown Menu</h2>
          <h3>Horizontal</h3>
          <foundation-dropdown-menu content={[
            {
              "url": "#0",
              "title": "Item 1",
              "submenu": [
                {
                  "url": "#0",
                  "title": "Item 1A",
                  "submenu": [
                    {
                      url: "#0",
                      title: "Item 1Aa"
                    },
                    {
                      url: "#0",
                      title: "Item 1Ba"
                    },
                    {
                      url: "#0",
                      title: "Item 1Ca"
                    },
                    {
                      url: "#0",
                      title: "Item 1Da"
                    },
                    {
                      url: "#0",
                      title: "Item 1Ea"
                    }
                  ]
                },
                {
                  "url": "#0",
                  "title": "Item 1B"
                },
                {
                  "url": "#0",
                  "title": "Item 1C"
                },
                {
                  "url": "#0",
                  "title": "Item 1D"
                },
                {
                  "url": "#0",
                  "title": "Item 1E"
                }
              ]
            },
            {
              "url": "#0",
              "title": "Item 2",
              "submenu": [
                {
                  url: "#0",
                  title: "Item 2A"
                },
                {
                  url: "#0",
                  title: "Item 2B"
                },
                {
                  url: "#0",
                  title: "Item 2C"
                },
                {
                  url: "#0",
                  title: "Item 2D"
                },
                {
                  url: "#0",
                  title: "Item 2E"
                }
              ]
            },
            {
              "url": "#0",
              "title": "Item 3",
              "submenu": [
                {
                  url: "#0",
                  title: "Item 3A"
                },
                {
                  url: "#0",
                  title: "Item 3B"
                },
                {
                  url: "#0",
                  title: "Item 3C"
                },
                {
                  url: "#0",
                  title: "Item 3D"
                },
                {
                  url: "#0",
                  title: "Item 3E"
                }
              ]
            },
            {
              "url": "#0",
              "title": "Item 4"
            },
          ]} />

          <h3>Vertical</h3>
          <foundation-dropdown-menu orientation="vertical" content={[
            {
              "url": "#0",
              "title": "Item 1",
              "submenu": [
                {
                  "url": "#0",
                  "title": "Item 1A",
                  "submenu": [
                    {
                      url: "#0",
                      title: "Item 1Aa"
                    },
                    {
                      url: "#0",
                      title: "Item 1Ba"
                    },
                    {
                      url: "#0",
                      title: "Item 1Ca"
                    },
                    {
                      url: "#0",
                      title: "Item 1Da"
                    },
                    {
                      url: "#0",
                      title: "Item 1Ea"
                    }
                  ]
                },
                {
                  "url": "#0",
                  "title": "Item 1B"
                },
                {
                  "url": "#0",
                  "title": "Item 1C"
                },
                {
                  "url": "#0",
                  "title": "Item 1D"
                },
                {
                  "url": "#0",
                  "title": "Item 1E"
                }
              ]
            },
            {
              "url": "#0",
              "title": "Item 2",
              "submenu": [
                {
                  url: "#0",
                  title: "Item 2A"
                },
                {
                  url: "#0",
                  title: "Item 2B"
                },
                {
                  url: "#0",
                  title: "Item 2C"
                },
                {
                  url: "#0",
                  title: "Item 2D"
                },
                {
                  url: "#0",
                  title: "Item 2E"
                }
              ]
            },
            {
              "url": "#0",
              "title": "Item 3",
              "submenu": [
                {
                  url: "#0",
                  title: "Item 3A"
                },
                {
                  url: "#0",
                  title: "Item 3B"
                },
                {
                  url: "#0",
                  title: "Item 3C"
                },
                {
                  url: "#0",
                  title: "Item 3D"
                },
                {
                  url: "#0",
                  title: "Item 3E"
                }
              ]
            },
            {
              "url": "#0",
              "title": "Item 4"
            },
          ]} />
        </section>

        <section>
          <h2>Dropdown Pane</h2>
          <foundation-dropdown-pane
            button="This is a button"
            content={`This is content`}
          />
        </section>

        <section>
          <h2>Equalizer</h2>
          <foundation-equalizer />
        </section>

        <section>
          <h2>Equalizer</h2>
          <foundation-equalizer />
        </section>


        <section>
          <h2>Form</h2>
          <foundation-form />
        </section>

        <section>
          <h2>Interchange</h2>
          <foundation-interchange />
        </section>

        <section>
          <h2>Label</h2>
          <foundation-label />
        </section>

        <section>
          <h2>Magellan</h2>
          <foundation-magellan />
        </section>

        <section>
          <h2>Media Object</h2>
          <foundation-media-object />
        </section>

        <section>
          <h2>Menu</h2>
          <foundation-menu />
        </section>

        <section>
          <h2>Off Canvas</h2>
          <foundation-off-canvas />
        </section>

        <section>
          <h2>Orbit</h2>
          <foundation-orbit />
        </section>

        <section>
          <h2>Pagination</h2>
          <foundation-pagination />
        </section>

        <section>
          <h2>Progress Bar</h2>
          <foundation-progress-bar />
        </section>

        <section>
          <h2>Responsive Embed</h2>
          <foundation-responsive-embed />
        </section>

        <section>
          <h2>Reveal</h2>
          <foundation-reveal />
        </section>

        <section>
          <h2>Slider</h2>
          <foundation-slider />
        </section>

        <section>
          <h2>Sticky</h2>
          <foundation-sticky />
        </section>

        <section>
          <h2>Switch</h2>
          <foundation-switch />
        </section>

        <section>
          <h2>Table</h2>
          <foundation-table />
        </section>

        <section>
          <h2>Tabs</h2>
          <foundation-tabs />
        </section>

        <section>
          <h2>Thumbnail</h2>
          <foundation-thumbnail />
        </section>

        <section>
          <h2>Tooltip</h2>
          <foundation-tooltip />
        </section>

        <section>
          <h2>Top Bar</h2>
          <foundation-top-bar />
        </section>
      </div >
    );
  }
}